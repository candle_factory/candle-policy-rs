package ru.mewory.einsurance.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import ru.mewory.einsurance.api.dto.EosagoCalculationRequest;
import ru.mewory.einsurance.api.dto.EosagoCalculationResponse;
import ru.mewory.einsurance.api.dto.OsagoDriverData;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EosagoRestControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void calculate() throws JsonProcessingException {
        String url = "http://localhost:" + port + "/calculateOsago";

        EosagoCalculationRequest request = new EosagoCalculationRequest();
        request.setArea("Москва");
        ArrayList<OsagoDriverData> driverData = new ArrayList<>();
        OsagoDriverData osagoDriverData = new OsagoDriverData();
        osagoDriverData.setAge(31);
        osagoDriverData.setExpirience(2);
        driverData.add(osagoDriverData);
        request.setDriverData(driverData);
        request.setHasViolations(false);
        request.setHorsePowers(100D);
        request.setKbm("3");
        request.setPartnerToken("test-partner-314");
        request.setTotalInsurancePeriod(12);

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(request));


        String json = "{\"area\":\"Москва\"," +
                        "\"totalInsurancePeriod\":12," +
                        "\"kbm\":\"3\"," +
                        "\"horsePowers\":100.0," +
                            "\"driverData\":[" +
                                    "{\"age\":31,\"expirience\":2}" +
                                "]," +
                        "\"partnerToken\":\"test-partner-314\"," +
                        "\"hasViolations\":false}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

        EosagoCalculationResponse eosagoCalculationResponse = restTemplate.postForObject(url, requestEntity, EosagoCalculationResponse.class);
        assertEquals(eosagoCalculationResponse.getCalculations().size(),2);
    }
}