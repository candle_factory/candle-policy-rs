package ru.mewory.einsurance.billing;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "partners")
public class Partner {

    @Id
    private Long id;
    private String name;
    private BigDecimal commisiion;

}
