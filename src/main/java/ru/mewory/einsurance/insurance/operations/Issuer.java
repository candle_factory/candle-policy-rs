package ru.mewory.einsurance.insurance.operations;

import ru.mewory.einsurance.insurance.entities.Contract;

public interface Issuer<T extends Contract> {

    T issue(T contract);

}
