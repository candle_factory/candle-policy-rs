package ru.mewory.einsurance.insurance.operations;

import ru.mewory.einsurance.insurance.entities.Contract;

public interface Calculator<T extends Contract> {
    T calculate(T c);
}
