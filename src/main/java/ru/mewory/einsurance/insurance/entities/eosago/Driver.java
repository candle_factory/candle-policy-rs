package ru.mewory.einsurance.insurance.entities.eosago;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
public class Driver implements Serializable {

    @Id
    private Long id;

    private Integer age;
    private Integer expirience;
}
