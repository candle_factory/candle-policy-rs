package ru.mewory.einsurance.insurance.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public abstract class Person implements Serializable {

    @Id
    public Long id;

}
