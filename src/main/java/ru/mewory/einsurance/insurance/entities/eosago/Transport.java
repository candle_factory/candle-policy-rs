package ru.mewory.einsurance.insurance.entities.eosago;

import lombok.Data;
import ru.mewory.einsurance.insurance.entities.InsuranceObject;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
@Data
public class Transport extends InsuranceObject implements Serializable {

    private String regNumber;
    private String vin;
    @OneToOne
    private TransportBrand brand;
    @OneToOne
    private TransportModel model;
    private Integer buildYear;
    private Double horsePowers;

}
