package ru.mewory.einsurance.insurance.entities.eosago;


import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.SerializationUtils;
import ru.mewory.einsurance.insurance.entities.Contract;

import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EOsagoContract extends Contract implements Serializable {

    private String area;

    @Override
    public String getProduct() {
        return "eosago";
    }

    @OneToMany
    @Getter
    @Setter
    private List<Driver> drivers = new ArrayList<>();

    public void setTransport(Transport transport){
        objects.clear();
        objects.add(transport);
    }

    public Transport getTransport(){
        if (objects.size() == 1) {
            return (Transport) objects.get(0);
        } else {
            return null;
        }
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public EOsagoContract getCopy(){
        return SerializationUtils.clone(this);
    }
}
