package ru.mewory.einsurance.insurance.entities;

public enum ContractStatus {
    CALCULATED, ISSUED
}
