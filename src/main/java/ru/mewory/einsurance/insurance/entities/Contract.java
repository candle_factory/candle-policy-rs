package ru.mewory.einsurance.insurance.entities;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.SerializationUtils;
import ru.mewory.einsurance.insurance.companies.abstr.InsuranceCompany;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class Contract implements Serializable {

    @OneToOne
    private InsuranceCompany company;

    private ContractStatus status;

    @Id
    private Long id;

    private BigDecimal premium;

    @OneToOne
    private Person insurer;

    @OneToMany
    protected List<InsuranceObject> objects = new ArrayList<>();

    public abstract String getProduct();

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public Person getInsurer() {
        return insurer;
    }

    public void setInsurer(Person insurer) {
        this.insurer = insurer;
    }

    public InsuranceCompany getCompany() {
        return company;
    }

    public void setCompany(InsuranceCompany company) {
        this.company = company;
    }

}
