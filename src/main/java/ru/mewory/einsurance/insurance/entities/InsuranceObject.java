package ru.mewory.einsurance.insurance.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public abstract class InsuranceObject implements Serializable {

    @Id
    private Long id;

}
