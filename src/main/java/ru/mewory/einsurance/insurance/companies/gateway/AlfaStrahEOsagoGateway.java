package ru.mewory.einsurance.insurance.companies.gateway;

import org.springframework.stereotype.Component;
import ru.mewory.einsurance.insurance.entities.ContractStatus;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;
import ru.mewory.einsurance.insurance.operations.Calculator;
import ru.mewory.einsurance.insurance.operations.Issuer;

import java.math.BigDecimal;

@Component
public class AlfaStrahEOsagoGateway implements Calculator<EOsagoContract>, Issuer<EOsagoContract> {

    @Override
    public EOsagoContract calculate(EOsagoContract contract) {
        contract.setPremium(new BigDecimal(100500D));
        return contract;
    }

    @Override
    public EOsagoContract issue(EOsagoContract contract) {
        contract.setStatus(ContractStatus.ISSUED);
        return contract;
    }
}
