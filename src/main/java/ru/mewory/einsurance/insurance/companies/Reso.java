package ru.mewory.einsurance.insurance.companies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mewory.einsurance.insurance.companies.abstr.EosagoSeller;
import ru.mewory.einsurance.insurance.companies.abstr.InsuranceCompany;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;
import ru.mewory.einsurance.insurance.companies.gateway.ResoEosagoGateway;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@Component
public class Reso extends InsuranceCompany implements EosagoSeller {

    @Autowired
    @Transient
    private ResoEosagoGateway gateway;

    @Override
    public String getName() {
        return "РЕСО";
    }

    @Override
    public EOsagoContract calculateEosago(EOsagoContract contract) {
        EOsagoContract calculate = gateway.calculate(contract);
        calculate.setCompany(this);
        return calculate;
    }

    @Override
    public EOsagoContract issueEosago(EOsagoContract contract) {
        if (this.equals(contract.getCompany())) {
            return gateway.issue(contract);
        } else {
            throw new RuntimeException("Расчет был произведен не этой компанией!");
        }    }
}
