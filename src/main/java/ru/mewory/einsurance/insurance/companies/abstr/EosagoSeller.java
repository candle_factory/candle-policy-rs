package ru.mewory.einsurance.insurance.companies.abstr;

import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

public interface EosagoSeller {

    EOsagoContract calculateEosago(EOsagoContract contract);
    EOsagoContract issueEosago(EOsagoContract contract);

}
