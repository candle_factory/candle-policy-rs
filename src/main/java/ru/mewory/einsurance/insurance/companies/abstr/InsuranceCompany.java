package ru.mewory.einsurance.insurance.companies.abstr;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public abstract class InsuranceCompany {

    @Id
    private Long id;
    public abstract String getName();

}
