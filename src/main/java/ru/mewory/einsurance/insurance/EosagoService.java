package ru.mewory.einsurance.insurance;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mewory.einsurance.insurance.companies.abstr.EosagoSeller;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import java.util.ArrayList;
import java.util.List;

@Service
public class EosagoService {

    @Autowired
    private List<EosagoSeller> eosagoSellers;

    public List<EOsagoContract> calculate(EOsagoContract contract){
        List<EOsagoContract> result = new ArrayList<>();
        for (EosagoSeller seller : eosagoSellers){
            result.add(seller.calculateEosago(contract.getCopy()));
        }
        return result;
    }

}
