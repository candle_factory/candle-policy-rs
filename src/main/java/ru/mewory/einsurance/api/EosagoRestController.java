package ru.mewory.einsurance.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mewory.einsurance.api.dto.EosagoCalculationRequest;
import ru.mewory.einsurance.api.dto.EosagoCalculationResponse;
import ru.mewory.einsurance.insurance.EosagoService;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import java.util.List;

@RestController
public class EosagoRestController {

    @Autowired
    EosagoService eosagoService;

    @GetMapping("/test")
    public String test(){
        eosagoService.calculate(null);
        return "";
    }

    @PostMapping("/calculateOsago")
    public ResponseEntity<EosagoCalculationResponse> calculate(@RequestBody EosagoCalculationRequest calculationRequest){
        List<EOsagoContract> calculate = eosagoService.calculate(calculationRequest.asContract());
        EosagoCalculationResponse calculationResponse = new EosagoCalculationResponse(calculate);
        return new ResponseEntity<>(calculationResponse, HttpStatus.OK);
    }

}
