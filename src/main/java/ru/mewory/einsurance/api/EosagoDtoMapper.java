package ru.mewory.einsurance.api;

import ru.mewory.einsurance.api.dto.EosagoCalculationRequest;
import ru.mewory.einsurance.api.dto.OsagoDriverData;
import ru.mewory.einsurance.insurance.entities.eosago.Driver;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;
import ru.mewory.einsurance.insurance.entities.eosago.Transport;

import java.util.ArrayList;
import java.util.List;

public class EosagoDtoMapper {

    public static EOsagoContract map(EosagoCalculationRequest request) {
        EOsagoContract contract = new EOsagoContract();
        contract.setArea(request.getArea());
        contract.setDrivers(mapDrivers(request.getDriverData()));
        contract.setTransport(mapTransport(request));
        return contract;
    }

    private static Transport mapTransport(EosagoCalculationRequest request) {
        Transport transport = new Transport();
        transport.setHorsePowers(request.getHorsePowers());
        return transport;
    }

    private static List<Driver> mapDrivers(List<OsagoDriverData> driverData) {
        List<Driver> result = new ArrayList<>();
        for (OsagoDriverData d : driverData){
            Driver driver = new Driver();
            driver.setAge(d.getAge());
            driver.setExpirience(d.getExpirience());
            result.add(driver);
        }
        return result;
    }
}
