package ru.mewory.einsurance.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import java.util.ArrayList;
import java.util.List;

@Data
public class EosagoCalculationResponse {

    @Getter
    @Setter
    private List<SingleCalculation> calculations = new ArrayList<>();

    public EosagoCalculationResponse(List<EOsagoContract> calculated) {
        for (EOsagoContract contract : calculated){
            calculations.add(new SingleCalculation(contract));
        }
    }

}
