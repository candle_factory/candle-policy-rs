package ru.mewory.einsurance.api.dto;

import lombok.Data;
import ru.mewory.einsurance.api.EosagoDtoMapper;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import java.util.ArrayList;
import java.util.List;

@Data
public class EosagoCalculationRequest {
    private String area;
    private int totalInsurancePeriod;
    private String kbm;
    private Double horsePowers;
    private List<OsagoDriverData> driverData = new ArrayList<>();
    private String partnerToken;
    private Boolean hasViolations;


    public EOsagoContract asContract() {
        return EosagoDtoMapper.map(this);
    }
}
