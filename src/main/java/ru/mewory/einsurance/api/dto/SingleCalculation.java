package ru.mewory.einsurance.api.dto;

import lombok.Data;
import ru.mewory.einsurance.insurance.entities.eosago.EOsagoContract;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SingleCalculation implements Serializable {
    String companyName;
    BigDecimal resultPremium;
    String message;

    SingleCalculation(EOsagoContract contract) {
        this.companyName = contract.getCompany().getName();
        this.resultPremium = contract.getPremium();
        this.message = "TODO";
    }
}
