package ru.mewory.einsurance.api.dto;

import lombok.Data;
import lombok.Getter;

@Data
public class OsagoDriverData {

    private int age;
    private int expirience;

}
